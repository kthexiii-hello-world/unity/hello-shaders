﻿Shader "Unlit/RayMarch_Shapes"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            #define MAX_STEPS 100
            #define MAX_DIST 100.
            #define SURF_DIST 1e-3

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 ro : TEXCOORD1;
                float3 hitPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
                o.hitPos =  v.vertex;
                return o;
            }

            float sdCapsule(float3 p, float3 a, float3 b, float r) {
                float3 ab = b - a;
                float3 ap = p - a;

                float t = dot(ab, ap) / dot(ab, ab);
                t = clamp(t, 0, 1);

                float3 c = a + t * ab;
                return length(p - c) - r;
            }

            float sdTorus(float3 p, float2 r){
                float x = length(p.xz) - r.x;
                return length(float2(x, p.y)) - r.y;
            }

            float sdBox(float3 p, float3 s){
                return length(max(abs(p)-s, 0));
            }

            float sdCylinder(float3 p, float3 a, float3 b, float r){
                float3 ab = b - a;
                float3 ap = p - a;

                float t = dot(ab, ap) / dot(ab, ab);

                float3 c = a + t * ab;
                float x = length(p - c) - r;
                float y = (abs(t - .5) - .5) * length(ab);
                float e = length(max(float2(x, y), 0)); // Exterior distance
                float i = min(max(x, y), 0); // Interior distance, performance penalty
                return e + i;
            }

            float GetDist(float3 p){
                float4 s = float4(0, 1, 6, 1); // x, y, z, radius
                float sphereDist = length(p - s.xyz) - s.w;
                float planeDist = p.y;

                float cd = sdCapsule(p, float3(0, .5, .4), float3(.1, .2, .3), .1);
                float td = sdTorus(p - float3(0, 0.1, 0), float2(.12, .05));
                float bd = sdBox(p - float3(.4, .1, -.2), .1);
                float cyd = sdCylinder(p, float3(0, .1, .5 ), float3(-.5, .1, .10), .1);

                float d = min(cd, planeDist);
                d = min(d, td);
                d = min(d, bd);
                d = min(d, cyd);

               return d;
            }

            float RayMarch(float3 ro, float3 rd) {
                float dO = 0;
                float dS;
                
                for(int i = 0; i < MAX_STEPS; i++){
                    float3 p = ro + rd * dO;
                    dS = GetDist(p);
                    dO += dS;
                    if(dO > MAX_DIST || dS < SURF_DIST) break;
                }
                return dO;
            }

            float3 GetNormal(float3 p){
                float d = GetDist(p);
                float2 e = float2(0.01, 0);

                float3 n = d - float3(
                    GetDist(p - e.xyy),
                    GetDist(p - e.yxy),
                    GetDist(p - e.yyx));
                
                return normalize(n);
            }

            float GetLight(float3 p){
                float3 lightPos = float3(0, 1, .2);
                lightPos.xz += float2(sin(_Time.y), cos(_Time.y)) * .5;
                float3 l = normalize(lightPos - p);
                float3 n = GetNormal(p);

                float dif = clamp(dot(n, l), 0, 1);
                float d = RayMarch(p + n * SURF_DIST * 4, l);
                if(d < length(lightPos - p)) dif *= .1;

                return dif;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - 0.5;
                // float3 ro = float3(0, 3, 0);
                // float3 rd = normalize(float3(uv.x, uv.y - .4, 1));
                float3 ro = i.ro; // float3(0,0,-3);
                float3 rd = normalize(i.hitPos - ro);// normalize(float3(uv.x, uv.y, 1));

                fixed4 col = 0;

                float d = RayMarch(ro, rd);

                float3 p = ro + rd *d;
                float dif = GetLight(p); // diffuse lightning

                col = dif;
                col.a = 1;
                return col;
            }
            ENDCG
        }
    }
}
