﻿Shader "Unlit/Value_Noise"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float N21(float2 p) {
                return frac(sin(p.x * 471 + p.y * 345) * 5647);
            }

            float SmoothNoise(float2 uv){
                float2 lv = frac(uv); // local uv coordinate
                float2 id = floor(uv); // local uv grid id

                lv = lv*lv*(3. - 2. * lv);
                float bl = N21(id); // bottom left
                float br = N21(id+float2(1,0)); // bottom right
                float b = lerp(bl, br, lv.x);

                float tl = N21(id + float2(0, 1)); // bottom left
                float tr = N21(id + float2(1, 1)); // bottom right
                float t = lerp(tl, tr, lv.x);
                
                return lerp(b, t, lv.y);
            }

            float SmoothNoise2(float2 uv){
                float c = SmoothNoise(uv*4);
                c += SmoothNoise(uv*8) * .5; // octave of noise
                c += SmoothNoise(uv*16) * .25;
                c += SmoothNoise(uv*32) * .125;
                c += SmoothNoise(uv*64) * .0625;

                return c / 2;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - .5;

                uv += _Time.y * .1;
                float c = SmoothNoise2(uv);
                
                fixed4 col = c;

                return col;
            }
            ENDCG
        }
    }
}
