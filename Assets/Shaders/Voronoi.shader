﻿Shader "Unlit/Voronoi"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[Toggle] _Naive ("Naive Voronoi", int) = 0 
		_Size ("Size", float) = 8
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			int _Naive = 0;
			float _Size = 8;
				
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            float2 N22(float2 p) {
				float3 a = frac(p.xyx * float3(535.34, 234.34, 345.65));
				a += dot(a, a + 34.45);
				return frac(float2(a.x * a.y, a.y * a.z));
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - .5;

				float m = 0;
				float t = _Time.y * .5;

				float minDist = 100;
				float cellIndex = 0;

				fixed4 col = 0;

				if(_Naive){
					for(float i = 0; i < 50; i++){
						float2 n = N22(i);
						float2 p = sin(n * t);

						float d = length(uv - p);
						m += smoothstep(.02, .01, d);

						if(d < minDist) {
							minDist = d;
							cellIndex = i;
						}
						col = minDist;
					}
				} else {
					uv *= _Size;
					float2 gv = frac(uv) - .5; // Grid UV
					float2 id = floor(uv);\
					float2 cid = 0;

					for(float y = -1; y <= 1; y++){
						for(float x = -1; x <= 1; x++){
							float2 offs = float2(x, y);
							
							float2 n = N22(id + offs);
							float2 p = offs + sin(n * t) * .5;
							p -= gv;
							float ed = length(p); // euclidean distance
							// float md = abs(p.x) + abs(p.y); // manhattan distance
							// float d = lerp(ed, md, sin(_Time.y) * .5 + .5);
							float d = ed;
							
							if(d < minDist) {
								minDist = d;
								cid = id+offs;
							}	

						}
					}

					col = minDist;
				}

                return col;
            }
            ENDCG
        }
    }
}
